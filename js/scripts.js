
// slider
// https://www.kirupa.com/html5/creating_a_sweet_content_slider.htm

// wrapperElm - wrapper se sirkou = souctu sirek vsech elementu
// dotsElms - ovladaci tecky
var wrapperElm = document.querySelector("#slider-wrapper");
var dotsElms = document.querySelectorAll(".dots");
var arrowsElms = document.querySelectorAll(".arrows");
var positions = ["0px", "-619px", "-1238px", "-1857px"];

// aktualne zobrazeny slide
var activeSlide = 0;

// nastaveni event listeners na klik na kazdou z tecek
// vola funkci setClickedItem
for (var i = 0; i < dotsElms.length; i++) {
    var dot = dotsElms[i];
    dot.addEventListener('click', setClickedItem, false);

    // itemid je globalni identifikator html prvku
    dot.itemID = i;
}

for (var j = 0; j < arrowsElms.length; j++) {
    var arrow = arrowsElms[j];
    arrow.addEventListener('click', nextItem, false);

    // itemid je globalni identifikator html prvku
    // arrow.itemID = i;
}

// set first item as active
dotsElms[activeSlide].classList.add("active");

function nextItem(e) {
  // odstranim active tridu z tecek
  removeActiveDots();

  var clickedArrow = e.target;

  if(clickedArrow.classList.contains('arrow-next')) {
    activeSlide += 1;
    if(activeSlide > 3) {
      activeSlide = 0;
    }
  } else if (clickedArrow.classList.contains('arrow-prev')) {
    activeSlide -= 1;
    if(activeSlide < 0) {
      activeSlide = 3;
    }
  }

  // nastavim translate hodnotu
  var position = positions[activeSlide];
  console.log(position);
  var translateValue = "translate(" + position + ", 0px)";
  wrapperElm.style.transform = translateValue;

  // na tecku pridam tridu active a tim ji pridam pozadovane styly
  var activeDot = 'dot-' + (activeSlide+1);
  document.getElementsByClassName(activeDot)[0].classList.add("active");
}

function nextItemHammer(direction) {
  // odstranim active tridu z tecek
  removeActiveDots();

  // var clickedArrow = e.target;

  if(direction == "next") {
    activeSlide += 1;
    if(activeSlide > 3) {
      activeSlide = 0;
    }
  } else if (direction == "prev") {
    activeSlide -= 1;
    if(activeSlide < 0) {
      activeSlide = 3;
    }
  }

  // nastavim translate hodnotu
  var position = positions[activeSlide];
  console.log(position);
  var translateValue = "translate(" + position + ", 0px)";
  wrapperElm.style.transform = translateValue;

  // na tecku pridam tridu active a tim ji pridam pozadovane styly
  var activeDot = 'dot-' + (activeSlide+1);
  document.getElementsByClassName(activeDot)[0].classList.add("active");
}


// zobrazeni zvoleneho slide podle toho, na kterou tecku
// jsme klikli
function setClickedItem(e) {
    // odstranim active tridu z tecek
    removeActiveDots();

    // clickedDot - tecka, na kterou jsme klikli
    // kazda tecka ma globalni id = 1/2/3/4, tuto hodnotu
    // nastavim do promenne activeSlide = ktery slide se zobrazi
    var clickedDot = e.target;
    activeSlide = clickedDot.itemID;

    // volam funkci changePosition, ktera zajisti posunuti na
    // zvoleny slide
    changePosition(clickedDot);
}

// odstraneni .active tridy
function removeActiveDots() {
    for (var i = 0; i < dotsElms.length; i++) {
        dotsElms[i].classList.remove("active");
    }
}


// funkce pro zobrazeni zvoleneho slide a zvyrazneni tecky
// na kterou jsme klikli
function changePosition(dot) {
    // pozici ziskam z data-pos atributu na zvolene tecce
    var position = dot.getAttribute("data-pos");

    // nastavim translate hodnotu
    var translateValue = "translate(" + position + ", 0px)";
    wrapperElm.style.transform = translateValue;

    // na tecku pridam tridu active a tim ji pridam pozadovane styly
    dot.classList.add("active");
}

// hammer.js
var sliderContent = document.getElementsByClassName('slider-with-navs');

for(var i = 0; i < sliderContent.length; i++) {
  var hammerSlider = new Hammer(sliderContent[i]);

  hammerSlider.on('swiperight', function(event) {
    console.log('swiperight');
    console.log(activeSlide);
    nextItemHammer('prev');
  });

  hammerSlider.on('swipeleft', function(event) {
    console.log('swipeleft');
    console.log(activeSlide);
    nextItemHammer('next');


  });
}


